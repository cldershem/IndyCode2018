const path = require('path');
const ffi = require('ffi');

const libhello = path.resolve(__dirname, './target/release/libhello_js')

const funcs = ffi.Library(libhello, {
  add_random_numbers: ['int', ['int', 'int']]
});

console.log(funcs.add_random_numbers(1, 7));

console.log();

function add_random_numbers_js(num1, num2) {
  console.log("JAVASCRIPT!");
  random_num = Math.floor(Math.random() * Math.floor(100));

  console.log(`${num1} + ${num2} + ${random_num}`);

  return num1 + num2 + random_num;
}

console.log(add_random_numbers_js(1, 7));
