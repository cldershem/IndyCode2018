from cffi import FFI


ffi = FFI()

ffi.cdef("""
    int add_random_numbers(int, int);
""")


C = ffi.dlopen("./target/release/libhello_js.so")

print()
answer = C.add_random_numbers(1, 7)

print("PYTHON!")
print(answer)
