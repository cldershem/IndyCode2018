extern crate rand;

#[no_mangle]
pub extern "C" fn add_random_numbers(num1: i32, num2: i32) -> i32 {
    println!("RUST!");
    let random_num = rand::random::<i32>();

    println!("{} + {} + {}", num1, num2, random_num);

    num1 + num2 + random_num
}
